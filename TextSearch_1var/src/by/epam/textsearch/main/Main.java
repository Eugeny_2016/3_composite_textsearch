package by.epam.textsearch.main;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.textsearch.entity.CompositeType;
import by.epam.textsearch.entity.TextComponent;
import by.epam.textsearch.report.Reporter;
import by.epam.textsearch.service.CalculatingService;
import by.epam.textsearch.service.ParsingService;
import by.epam.textsearch.service.ReadingService;
import by.epam.textsearch.service.SelectingService;
import by.epam.textsearch.service.WritingService;

public class Main {
	
	private static final Logger LOG = Logger.getLogger(Main.class);
	
	// ����������� ���� ��� ����� ����� ������ � ������ ������ 
	private static final String SOURCE_FRAGMENT_PATH = "source/fragment.txt";
	private static final String RESULT_TEXT_COLLECTION_PATH = "result/textCollection.txt";
	private static final String RESULT_PARAGRAPH_COLLECTION_PATH = "result/paragraphCollection.txt";
	private static final String RESULT_LISTING_COLLECTION_PATH = "result/listingCollection.txt";
	private static final String RESULT_SENTENCE_COLLECTION_PATH = "result/sentenceCollection.txt";
	private static final String RESULT_WORD_COLLECTION_PATH = "result/wordCollection.txt";
	
	public static void main(String[] args) {
		
		long before = System.currentTimeMillis();		// �������� �����
		
		// ��������� ��� ������ ����������� �������� � �����
		ArrayList<String> textCollection = new ArrayList<String>();
		ArrayList<String> paragraphCollection = new ArrayList<String>();
		ArrayList<String> listingCollection = new ArrayList<String>();
		ArrayList<String> sentenceCollection = new ArrayList<String>();
		ArrayList<String> wordCollection = new ArrayList<String>();
		
		// ������ ��������� ���������, ������� ����� ��������
		TextComponent textComponent = null;
		
		// ������ ������ ��������� ��������� ���������� ������ (���������, �����������, �����). ��� ��� ����� �������� �� ������������ ���������� ����������. 
		List<TextComponent> listOfParagraphs = null;
		List<TextComponent> listOfSentences = null;
		List<TextComponent> listOfWords = null;
		
		// ������ ��������� �������� �� ����� 
		String fragment = ReadingService.readFile(SOURCE_FRAGMENT_PATH);
		
		// ������ ��������� ��������. ��� ����� �������� 1-� ������ ������-������� � ��� ������������ ����������� �� ����� �����, � ����� ������ ��������� ��� ����������� ������ � �����
		ParsingService service = new ParsingService(CompositeType.TEXT, textCollection, paragraphCollection, listingCollection, sentenceCollection, wordCollection);
		textComponent = service.parseText(fragment);
		
		// ���������� ����������� ��������� � �����
		WritingService.writeFile(textCollection, paragraphCollection, listingCollection, sentenceCollection, wordCollection, 
				RESULT_TEXT_COLLECTION_PATH, RESULT_PARAGRAPH_COLLECTION_PATH, RESULT_LISTING_COLLECTION_PATH, RESULT_SENTENCE_COLLECTION_PATH, RESULT_WORD_COLLECTION_PATH);
		
		// �������� ������ ������ ���� ����������, �����������, ���� ������ ���������� ���������
		listOfParagraphs = SelectingService.selectAllParagraphs(textComponent.getChilds());
		listOfSentences = SelectingService.selectAllSentences(listOfParagraphs);
		listOfWords = SelectingService.selectAllWords(listOfSentences);
		
		// ������� �� ����� ��������������� ����� � ���������� ����������� � ���
		Reporter.displayAmountOfTextComponents(textComponent);
		Reporter.displayAmountOfEveryComponent(listOfParagraphs, listOfSentences, listOfWords);
		Reporter.displayTextComponents(textComponent);		
					
		// �������� ������ �������, ������������ �������������� ����������� �� �������
		// ����� ������� ���������� �� ����� �������� ������-���������
		Reporter.displayAmountOfSentencesWithSameWords(CalculatingService.countSentencesWithSameWords(listOfSentences)); // #1
		Reporter.displaySentencesSortedByAmountOfWords(CalculatingService.sortByAmountOfWordsAscendig(listOfSentences)); // #2
		
		long after = System.currentTimeMillis();
		long difference = after - before;
		LOG.info("Programm running time is: " + difference + " ms");				// ��������� ����� ���������� ���������
	}
}

// ����������� �� ���������� �����:
//------------------------------------------------------------------------------------------------------------------------------
// ����� ������ ���������� � � �������� �� ����� ������(\n) � 2-� ��������. ����� ����� ������ �������������.
// ������ ����� ������ ���������� � 1-��� � ����� ��������� �� ����� ������(\n) � 2-� ��������
// ������ ������� ������ ���������� � 1-��� � ����� ��������� �� ����� ������(\n), 2-� �������� � ������ ������� ��������� (\t)
// ����� ������� ������� ��������� ����������� (�����, �����. �����, ����. �����, ���������) ������ ������ 1-� � ����� �������� ��� ��������� �� ����� ������ 
// ���� ������� (. ? ! :) �������� ������ �����������, �� ����� ��� �� ������ ������ ���������� ��������. ����� �� ��� ��������� ������ �����������.
