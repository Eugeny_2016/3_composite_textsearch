package by.epam.textsearch.report;

import java.util.List;

import by.epam.textsearch.entity.TextComponent;

public class Reporter {
	
	public static final String DELIMETERS = 
			"--------------------------------------------------------------------------------------------------------------------------------------------------------------------------";
	
	public static void displayAmountOfTextComponents(TextComponent textComponent) {
		System.out.print(DELIMETERS + "\n");
		System.out.print("The amount of components in text-component is: " + textComponent.getChilds().size() + "\n");
	}
	
	public static void displayAmountOfEveryComponent(List<TextComponent> listOfParagraphs, List<TextComponent> listOfSentences, List<TextComponent> listOfWords) {
		System.out.print(DELIMETERS + "\n");
		System.out.print("The amount of paragraphs is: " + listOfParagraphs.size() + "\n");
		System.out.print("The amount of sentences is: " + listOfSentences.size() + "\n");
		System.out.print("The amount of words is: " + listOfWords.size() + "\n");
	}
	
	public static void displayTextComponents(TextComponent textComponent) {
		System.out.print(DELIMETERS + "\n");
		System.out.print("Your text-component is: \n");
		System.out.print(DELIMETERS + "\n");
		System.out.println(textComponent);
	}
	
	public static void displayAmountOfSentencesWithSameWords(int amountOfSentences) {
		System.out.print(DELIMETERS + "\n");
		System.out.print("***The amount of coincidences of words in all sentences is: " + amountOfSentences + "\n");
	}
	
	public static void displaySentencesSortedByAmountOfWords(List<TextComponent> listOfSentences) {
		System.out.print(DELIMETERS + "\n");
		System.out.print("***Sentences after sorting in ascending order by the amount of words in them: \n");
		System.out.print(DELIMETERS + "\n");
		for (TextComponent component : listOfSentences) {
			System.out.print(component.toString().trim().replace('\n', ' ') + "\n");			// ��� ������ �� ������� ������� ��� ������ ���������� ������� � ���������� (� ������, � �����, ������) 
		}
	}

}
