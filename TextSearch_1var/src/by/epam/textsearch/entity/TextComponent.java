package by.epam.textsearch.entity;

import java.util.List;

public interface TextComponent {
	int countComponents();
	int countSymbols();
	List<TextComponent> getChilds();
	TextComponent getChild(int position);
}
