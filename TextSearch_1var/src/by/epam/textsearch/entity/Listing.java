package by.epam.textsearch.entity;

import java.util.List;

public final class Listing implements TextComponent {
	
	private String codeListing;
	
	public Listing(String codeListing) {
		this.codeListing = codeListing;
	}
	
	public String getCodeListing() {
		return codeListing;
	}

	@Override
	public String toString() {
		return codeListing;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		
		if (this == obj) {
			return true;
		}
		
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		
		Listing listing = (Listing) obj;
		
		if (!codeListing.equals(listing.codeListing)) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public int countSymbols() {
		return 0;
	}
	
	@Override
	public int countComponents() {
		return 0;
	}

	@Override
	public TextComponent getChild(int position) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<TextComponent> getChilds() {
		throw new UnsupportedOperationException();
	}
}
