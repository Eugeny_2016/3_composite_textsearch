package by.epam.textsearch.entity;

public enum CompositeType {
	TEXT, PARAGRAPH, SENTENCE, WORD
}
