package by.epam.textsearch.entity;

import java.util.List;

public final class Symbol implements TextComponent {
	
	private SymbolType symbolType;
	private char character;
	
	public Symbol(SymbolType symbolType, char character) {
		this.symbolType = symbolType;
		this.character = character;
	}
	
	public SymbolType getSymbolType() {
		return symbolType;
	}

	public char getCharacter() {
		return character;
	}

	@Override
	public String toString() {
		return String.valueOf(character);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		
		if (this == obj) {
			return true;
		}
		
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		
		Symbol symbol = (Symbol) obj;
		
		if (!symbolType.equals(symbol.symbolType)) {
			return false;
		}
		
		if (character != symbol.character) {
			return false;
		}
				
		return true;
	}
	
	@Override
	public int countSymbols() {
		return 1;
	}
	
	@Override
	public int countComponents() {
		return 0;
	}

	@Override
	public TextComponent getChild(int position) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<TextComponent> getChilds() {
		throw new UnsupportedOperationException();
	}
}
