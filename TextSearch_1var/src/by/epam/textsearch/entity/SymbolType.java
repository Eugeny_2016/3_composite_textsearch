package by.epam.textsearch.entity;

public enum SymbolType {
	ALPHANUMERIC, PUNCTUATION, BLANK
}