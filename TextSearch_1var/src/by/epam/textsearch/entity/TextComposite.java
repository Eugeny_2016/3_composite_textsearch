package by.epam.textsearch.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TextComposite implements TextComponent {
	
	private CompositeType compositeType;
	private ArrayList<TextComponent> textComponents = new ArrayList<TextComponent>();
	
	public TextComposite(CompositeType compositeType) {
		this.compositeType = compositeType;
	}
	
	public CompositeType getCompositeType() {
		return compositeType;
	}

	public void add(TextComponent component) {
		textComponents.add(component);
	}

	public void remove(TextComponent component) {
		textComponents.remove(component);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (TextComponent component : textComponents) {
			sb.append(component.toString());
		}
		return sb.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		
		if (this == obj) {
			return true;
		}
		
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		
		TextComposite composite = (TextComposite) obj;
				
		if (textComponents.size() != composite.getChilds().size()) {		// ���� ��������� ������ �����, �� ���������� ����
			return false;
		}
				
		for (int i = 0; i < textComponents.size(); i++) {
			if (!textComponents.get(i).equals(composite.getChild(i))) {		// ���������� ArrayOutOfBoundsException ���� �� ������, ��� ���� ��� �������� �� ��������� ���� ����
				return false;												// ���� ���� ���� ��������� ���� ���� ����, ���������� ����
			}
		}
		
		return true;
	}
	
	@Override
	public int countSymbols() {
		int amountOfSymbols = 0;
		for (TextComponent component : textComponents) {
			amountOfSymbols = amountOfSymbols + component.countSymbols();
		}
		return amountOfSymbols;
	}

	@Override
	public int countComponents() {
		int amountOfComponents = 0;
		for (TextComponent component : textComponents) {
			amountOfComponents = amountOfComponents + component.countComponents();
		}
		return (amountOfComponents + 1);
	}

	@Override
	public List<TextComponent> getChilds() {
		return Collections.unmodifiableList(textComponents);
	}
	
	@Override
	public TextComponent getChild(int position) {
		return textComponents.get(position);
	}
}
