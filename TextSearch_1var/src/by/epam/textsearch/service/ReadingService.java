package by.epam.textsearch.service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ReadingService {
	
	public static String readFile(String fileName) {
		StringBuilder fragment = new StringBuilder();
		try {
			FileReader fr = new FileReader(fileName);
			BufferedReader br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null) {
				fragment.append(line + "\n");
			}
			br.close();
			fr.close();
		} catch (FileNotFoundException e) {
			throw new RuntimeException("File wasn't found!");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return fragment.toString();
	}
}
