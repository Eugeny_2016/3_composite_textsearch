package by.epam.textsearch.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import by.epam.textsearch.entity.TextComponent;

public class CalculatingService {

// �����, ����������� ���������� ����������� ������ � ����������� ������� 
	public static int countSentencesWithSameWords(List<TextComponent> listOfSentences) {
		TextComponent firstSentence = null, secondSentence = null;
		List<TextComponent> firstListOfWords = new ArrayList<TextComponent>();
		List<TextComponent> secondListOfWords = new ArrayList<TextComponent>();
		int amountOfSentencesWithSameWords = 0;
		
		for (int i = 0; i < listOfSentences.size(); i++) {
			firstSentence = listOfSentences.get(i);
			firstListOfWords = SelectingService.selectWords(firstSentence);
			for (int j = i + 1; j < listOfSentences.size(); j++) {
				secondSentence = listOfSentences.get(j);
				secondListOfWords = SelectingService.selectWords(secondSentence);
				if (compareWords(firstListOfWords, secondListOfWords)) {
					amountOfSentencesWithSameWords++;								// � ������ ���������� ������� ����������� ������� �� �������
				}				
			}
		}		
		return amountOfSentencesWithSameWords;
	}
	
// ������� �����, ������ ������������� ��� ������ ������������ ������ 
	private static boolean compareWords(List<TextComponent> firstListOfWords, List<TextComponent> secondListOfWords) {
		boolean result = false;
		String firstWord = null, secondWord = null;
		for (int i = 0; i < firstListOfWords.size(); i++) {
			firstWord = firstListOfWords.get(i).toString().trim().replaceAll("\\p{Punct}", "");		// ������� ��� ���������� ������� � ������ � � �����, ������� ����������, �.�. ����� �������� ������ � ����
			for (int j = 0; j < secondListOfWords.size(); j++) {
				secondWord = secondListOfWords.get(j).toString().trim().replaceAll("\\p{Punct}", "");
				if (firstWord.equals(secondWord)) {													// ���������� ������ ����� 1-��� ����������� � ������ ������ 2-��� �����������
					return true;																	// ���� ���� ���� ���������� ������� ����� ������� ���� �����������, ���������� ������ � ������ ���� �� ���������
				}
			}
		}
		return result;																				// � ���� �� �������, �� �������� ����
	}
	
// �����, ������������ ������ ���� �����������, ��������������� � ������� ����������� ���������� ���� � ������ �� ��� 	
	public static List<TextComponent> sortByAmountOfWordsAscendig(List<TextComponent> listOfSentences) {
		List<TextComponent> sortedListOfSentences = new ArrayList<TextComponent>();
		listOfSentences.sort(new Comparator<TextComponent>() {
		
		@Override
		public int compare(TextComponent component1, TextComponent component2) {
			return (component1.getChilds().size() - component2.getChilds().size());
		}		
		});
		sortedListOfSentences.addAll(listOfSentences);
		return sortedListOfSentences;
	}

}
