package by.epam.textsearch.service;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import by.epam.textsearch.entity.TextComposite;
import by.epam.textsearch.entity.CompositeType;
import by.epam.textsearch.entity.Listing;
import by.epam.textsearch.entity.Symbol;
import by.epam.textsearch.entity.SymbolType;

public final class ParsingService {
	
	private static final Logger LOG = Logger.getLogger(ParsingService.class);
	
	// ��������� �������� ��� ���������� ���������
	private static final String LISTING_SEARCH_PATTERN = "(\t{1})(//|\\?|@|import|class|[a-zA-Z]+|[\\w]+\\.java)";
	private static final String TEXT_AFTER_LISTING_SEARCH_PATTERN = "(\n{2})(\u0020{2})";
	private static final String PARAGRAPH_SEARCH_PATTERN = "(\n+)(\u0020{2})";
	private static final String SENTENCE_SEARCH_PATTERN = "([\\.!?:]{1})([\n\u0020]+)";
	private static final String WORD_SEARCH_PATTERN = "([^\n\u0020]+)";
	private static final String PUNCTUATION_SEARCH = "(\\p{Punct})";
	private static final String BLANK_SEARCH = "([\n\u0020])";
	
	// ��������� �����
	private TextComposite textComposite;
	
	// ��������� ��� ������ ����������� �������� � ����
	private List<String> textCollection;
	private List<String> paragraphCollection;
	private List<String> listingCollection;
	private List<String> sentenceCollection;
	private List<String> wordCollection;
		
	public ParsingService(CompositeType compositeType, ArrayList<String> textCollection, ArrayList<String> paragraphCollection, 
			ArrayList<String> listingCollection, ArrayList<String> sentenceCollection, ArrayList<String> wordCollection) {
		
		textComposite = new TextComposite(compositeType);
		this.textCollection = textCollection;
		this.paragraphCollection = paragraphCollection;
		this.listingCollection = listingCollection;
		this.sentenceCollection = sentenceCollection;
		this.wordCollection = wordCollection;
	}
	
// ����� ��������� ����� ������ �� ��������� � ��������
	public TextComposite parseText(String fragment) {
		
		int startOfText = 0, endOfText = 0, startOfListing = 0, endOfListing = 0;
		boolean noListingsFlag = false;
		ArrayList<String> paragraphs = null;
		
		Pattern patternListing = null, patternText = null;
		Matcher matcherListing = null, matcherText = null;
				
		patternListing = Pattern.compile(LISTING_SEARCH_PATTERN);
		patternText = Pattern.compile(TEXT_AFTER_LISTING_SEARCH_PATTERN);
				
		// �������� ������ �����
		matcherListing = patternListing.matcher(fragment);												// ��� ����� ���� ������ ������� ��������
		if (matcherListing.find()) {																	// ���� ������� ������, �� �������� ������ ����� �� ��������
			endOfText = matcherListing.start();
			startOfListing = matcherListing.start();
			LOG.debug("Start of listing:" + startOfListing);
			LOG.debug("End of text:" + endOfText + " Ending element:" + matcherListing.group(1));
			paragraphs = parsePureText(fragment.substring(startOfText, endOfText));						// �������� ������ ���� ���������� ������� ������
			for (int i = 0; i < paragraphs.size(); i++) {
				LOG.debug(paragraphs.get(i));
				paragraphCollection.add(paragraphs.get(i) + "-->");										// ��������� �������� � ��������� ��� ����������� ������ � ����
				this.textComposite.add(parseParagraph(paragraphs.get(i)));								// � ������ �������� ����� ����������� � ����� �������� ���������� �� ������������� ����������� 
			}
		} else {																						// ���� �������� � ������ �� ���������, �� ������ ��� ���� ������ �����
			LOG.info("There are no listings in the text");
			paragraphs = parsePureText(fragment.substring(startOfText));								// �������� ������ ���� ���������� ����� ������
			for (int i = 0; i < paragraphs.size(); i++) {
				paragraphCollection.add(paragraphs.get(i) + "-->");										// ��������� �������� � ��������� ��� ����������� ������ � ����
				this.textComposite.add(parseParagraph(paragraphs.get(i)));								// � ������ �������� ����� ����������� � ����� �������� ���������� �� ������������� ����������� 
			}
			noListingsFlag = true;																		// ������� ����, ��������������� � ���, ��� ��������� � ������ �� ���������
		}
		
		// ���� � �������� ��� ��������� ������ � ��������, ����� ���������
		while (matcherListing.find()) {																	// ��� ����� ���� ������ ���������� ��������
			endOfText = matcherListing.start();
			LOG.debug("Start of new listing:" + endOfText);
			LOG.debug("End of text:" + endOfText + " Ending element:" + matcherListing.group(1));
			matcherText = patternText.matcher(fragment.substring(startOfListing, endOfText));
			
			if (matcherText.find()) {																	// ���� ������� ������, �� ���� ����� ����� ���� ���������				
				endOfListing = startOfListing + matcherText.start();									// ����������, ����� �������� � ������ ����� ����� ��������� ���������
				LOG.debug("End of old listing:" + endOfListing + " After listing end element:" + matcherText.group(1));
				startOfText = startOfListing + matcherText.start();
				listingCollection.add(fragment.substring(startOfListing, endOfListing) + "-->");		// ��������� ������� � ��������� ��� ����������� ������ � ����
				this.textComposite.add(new Listing(fragment.substring(startOfListing, endOfListing)));	// ��������� ������� � ����� �������� 
				paragraphs = parsePureText(fragment.substring(startOfText, endOfText));					// �������� ������ ���� ���������� ����� ������
				for (int i = 0; i < paragraphs.size(); i++) {
					LOG.debug(paragraphs.get(i));
					paragraphCollection.add(paragraphs.get(i) + "-->");									// ��������� �������� � ��������� ��� ����������� ������ � ����
					this.textComposite.add(parseParagraph(paragraphs.get(i)));							// � ������ �������� ����� ����������� � ����� �������� ���������� �� ������������� ����������� 
				}
			} else {																					// ���� ������ �� �����, ������ ��� ��� ������ �������, � �������  ������ ��������� ��� � ����� ��������
				endOfListing = matcherListing.start();
				listingCollection.add(fragment.substring(startOfListing, endOfListing) + "-->");		// ��������� ������� � ��������� ��� ����������� ������ � ����
				this.textComposite.add(new Listing(fragment.substring(startOfListing, endOfListing)));
			}
			startOfListing = matcherListing.start();
		}
		
		// ���� �������� ��������� ������� � ����� (������ ���� ���� �� ���������� ��������� � ������ �� ��� ������)
		if (!noListingsFlag) {
			LOG.debug("Start of last listing:" + startOfListing);
			matcherText = patternText.matcher(fragment.substring(startOfListing));
			if (matcherText.find()) {																	// ����, ���� �� ����� ���������� �������� �����
				endOfListing = startOfListing + matcherText.start();									// ���� ����, ��
				LOG.debug("End of last listing:" + endOfListing);
				startOfText = startOfListing + matcherText.start();
				LOG.debug("Start of last text:" + startOfText);
				listingCollection.add(fragment.substring(startOfListing, endOfListing) + "-->");		// ��������� ��������� ������� � ��������� ��� ����������� ������ � ����
				this.textComposite.add(new Listing(fragment.substring(startOfListing, endOfListing)));	// ��������� ��������� ������� � ����� ��������
				paragraphs = parsePureText(fragment.substring(startOfText));							// �������� ������ ���� ���������� ����� ������
				for (int i = 0; i < paragraphs.size(); i++) {
					LOG.debug(paragraphs.get(i));
					paragraphCollection.add(paragraphs.get(i) + "-->");									// ��������� �������� � ��������� ��� ����������� ������ � ����
					this.textComposite.add(parseParagraph(paragraphs.get(i)));							// � ������ �������� ����� ����������� � ����� �������� ���������� �� ������������� ����������� 
				}
			} else {																					// ���� ���������� ������ �� �����, ��
				listingCollection.add(fragment.substring(startOfListing, endOfListing) + "-->");		// ��������� ��������� ������� � ��������� ��� ����������� ������ � ����
				this.textComposite.add(new Listing(fragment.substring(startOfListing))); 				// ��������� ��������� ������� � ����� ��������
			}
		}
		
		textCollection.add(textComposite.toString());													// ��������� ������� �������� ������ � ��������� ������ ��� ����������� ������ � ����
		
		return textComposite;																			// ���������� ������� �������� ������						
	}
	
// ����� ��������� ������ �� ���������, ������������ ������ �����-����������
	private ArrayList<String> parsePureText(String fragment) {								
			int startOfParagraph = 0, endOfParagraph = 0;
			ArrayList<String> paragraphs = new ArrayList<String>();
			
			Pattern pattern = Pattern.compile(PARAGRAPH_SEARCH_PATTERN);
			Matcher matcher = pattern.matcher(fragment);
			matcher = pattern.matcher(fragment);														// � ������ ���� ���������
			matcher.find();																				// ���� ������ ��������
			startOfParagraph = matcher.start();
			
			while (matcher.find()) {																	// ������ ��������� �������� ���������� � ������ ����������  
				LOG.debug("Paragraph start:" + startOfParagraph);
				endOfParagraph = matcher.end() - 2;
				LOG.debug("Paragraph NormalEnd:" + matcher.end() + " NewEnd:" + endOfParagraph);
				paragraphs.add(fragment.substring(startOfParagraph, endOfParagraph));
				startOfParagraph = matcher.end() - 2;
			}
			LOG.debug("End of paragraphs array collecting");
			return paragraphs;
		}
	
// ����� ��������� ��������� �� �����������, ������������ �������� ��������
	private TextComposite parseParagraph(String fragment) {
		int startOfSentence = 0, endOfSentence = 0;
		Pattern pattern = null;
		Matcher matcher = null;
		TextComposite textComposite = new TextComposite(CompositeType.PARAGRAPH);
						
		pattern = Pattern.compile(SENTENCE_SEARCH_PATTERN);
		matcher = pattern.matcher(fragment);
		// ��������� ���������� �������� �� ����������� � ���������� ������ ����������� �� ������� ����
		while (matcher.find()) {
			endOfSentence = matcher.end();
			LOG.debug("Sentence:" + fragment.substring(startOfSentence,endOfSentence));
			sentenceCollection.add(fragment.substring(startOfSentence, endOfSentence) + "-->");			// ��������� ����������� � ��������� ��� ����������� ������ � ����
			textComposite.add(parseSentense(fragment.substring(startOfSentence, endOfSentence)));
			startOfSentence = matcher.end();
		}
		return textComposite;
	}
	
// ����� ��������� ����������� �� �����, ������������ �������� �����������
	private TextComposite parseSentense(String fragment) {
		int startOfWord = 0 , secondStartOfWord = 0;
		Pattern pattern = null;
		Matcher matcher = null;
		TextComposite textComposite = new TextComposite(CompositeType.SENTENCE);
		
		pattern = Pattern.compile(WORD_SEARCH_PATTERN);
		matcher = pattern.matcher(fragment);
		matcher.find();
		// ��������� ���������� ����������� �� ����� � ���������� ������ ����� �� ������� ��������
		while (matcher.find()) {
			secondStartOfWord = matcher.start();
			LOG.debug("Word:" + fragment.substring(startOfWord, secondStartOfWord));
			wordCollection.add(fragment.substring(startOfWord, secondStartOfWord) + "-->");				// ��������� ����� � ��������� ��� ����������� ������ � ����
			textComposite.add(parseWord(fragment.substring(startOfWord, secondStartOfWord)));
			startOfWord = matcher.start();
		}
		textComposite.add(parseWord(fragment.substring(secondStartOfWord)));							// ��� ������ ���������� ����� �����������
		return textComposite;
	}
	
// ����� ��������� ����� �� �������, ������������ �������� �����
	private TextComposite parseWord(String fragment) {
		TextComposite textComposite = new TextComposite(CompositeType.WORD);
		char[] array = fragment.toCharArray();
		Symbol symbol = null;
		
		for (int i = 0; i < array.length; i++) {
			String s = String.valueOf(array[i]);
			if (s.matches(PUNCTUATION_SEARCH)) {
				symbol = new Symbol(SymbolType.PUNCTUATION, array[i]);
				textComposite.add(symbol);
			} else if (s.matches(BLANK_SEARCH)){
				symbol = new Symbol(SymbolType.BLANK, array[i]);
				textComposite.add(symbol);
			} else {
				symbol = new Symbol(SymbolType.ALPHANUMERIC, array[i]);
				textComposite.add(symbol);
			}
		}		
		return textComposite;
	}
}
