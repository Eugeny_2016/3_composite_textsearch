package by.epam.textsearch.service;

import java.util.ArrayList;
import java.util.List;

import by.epam.textsearch.entity.Listing;
import by.epam.textsearch.entity.TextComponent;

public class SelectingService {

// �����, ������������ ������ ������ ���������� �� ������
	public static List<TextComponent> selectAllParagraphs(List<TextComponent> components) {
		List<TextComponent> listOfAllParagraphs = new ArrayList<TextComponent>();
		
		for (TextComponent component : components) {
			if (!Listing.class.equals(component.getClass())) {
				listOfAllParagraphs.add(component);
			}
		}
		return listOfAllParagraphs;
	}
	
// �����, ������������ ������ ������ ����������� �� ����������
	public static List<TextComponent> selectAllSentences(List<TextComponent> components) {
		List<TextComponent> listOfAllSentences = new ArrayList<TextComponent>();
				
		for (int i = 0; i < components.size(); i++) {
			for (TextComponent component : components.get(i).getChilds()) {	// �������� ������ ���� ����������� i-��� ��������� � ������ ����������� ��������� � ����� ������ �����������
				listOfAllSentences.add(component);
			}
		}
		return listOfAllSentences;
	}
	
// �����, ������������ ������ ������ ���� �� �����������
	public static List<TextComponent> selectAllWords(List<TextComponent> components) {
		List<TextComponent> listOfAllWords = new ArrayList<TextComponent>();
		
		for (int i = 0; i < components.size(); i++) {
			for (TextComponent component : components.get(i).getChilds()) {	// �������� ������ ���� ���� i-��� ����������� � ������ ����� ��������� � ����� ������ ����
				listOfAllWords.add(component);
			}
		}
		return listOfAllWords;
	}
// �����, ������������ ������ ������ ����� ������ ���������� �� ����� �������� ��������� (��� ����� ����������� ��� ��� ����������� ������)
	public static List<TextComponent> selectWords(TextComponent textComponent) {
		List<TextComponent> listOfWords = new ArrayList<TextComponent>();
				
		for (TextComponent component : textComponent.getChilds()) {	// �������� ������ ���� ���� i-��� ����������� � ������ ����� ��������� � ����� ������ ����
			listOfWords.add(component);
		}
		return listOfWords;
	}
}
