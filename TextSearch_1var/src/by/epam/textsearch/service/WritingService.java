package by.epam.textsearch.service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class WritingService {
	
	public static void writeFile(ArrayList<String> textCollection, ArrayList<String> paragraphCollection, ArrayList<String> listingCollection, 
			ArrayList<String> sentenceCollection, ArrayList<String> wordCollection, String resultTextCollectionPath, String resultParagraphCollectionPath, 
			String resultListingCollectionPath, String resultSentenceCollectionPath, String resultWordCollectionPath) {
		
		try {
			// ������ ���� ��� ������ ����� ������������� ������
			FileWriter textCollectionFile = new FileWriter(resultTextCollectionPath);
			BufferedWriter bufferText = new BufferedWriter(textCollectionFile);
			PrintWriter printText = new PrintWriter(bufferText);
			
			// ������ ���� ��� ������ ���� ����������
			FileWriter paragraphCollectionFile = new FileWriter(resultParagraphCollectionPath);
			BufferedWriter bufferParagraph = new BufferedWriter(paragraphCollectionFile);
			PrintWriter printParagraph = new PrintWriter(bufferParagraph);
			
			// ������ ���� ��� ������ ���� ���������
			FileWriter listingCollectionFile = new FileWriter(resultListingCollectionPath);
			BufferedWriter bufferListing = new BufferedWriter(listingCollectionFile);
			PrintWriter printListing = new PrintWriter(bufferListing);
			
			// ������ ���� ��� ������ ���� �����������
			FileWriter sentenceCollectionFile = new FileWriter(resultSentenceCollectionPath);
			BufferedWriter bufferSentence = new BufferedWriter(sentenceCollectionFile);
			PrintWriter printSentence = new PrintWriter(bufferSentence);
			
			// ������ ���� ��� ������ ���� ����
			FileWriter wordCollectionFile = new FileWriter(resultWordCollectionPath);
			BufferedWriter bufferWord = new BufferedWriter(wordCollectionFile);
			PrintWriter printWord = new PrintWriter(bufferWord);
			
			for (String line : textCollection) {
				printText.print(line);
			}
			
			for (String line : paragraphCollection) {
				printParagraph.print(line);
			}
			
			for (String line : listingCollection) {
				printListing.print(line);
			}
			
			for (String line : sentenceCollection) {
				printSentence.print(line);
			}
			
			for (String line : wordCollection) {
				printWord.print(line);
			}
			
			printText.close();
			printParagraph.close();
			printListing.close();
			printSentence.close();
			printWord.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
